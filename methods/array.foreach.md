# Array.forEach()

{% embed url="https://replit.com/@bgoonz/array-methods#index.js" %}

### forEach() <a href="96f8" id="96f8"></a>

```
const array = [1, 2, 3, 4]array.forEach((elemnt, index) => {   console.log(`Element ${element} at index ${index}`)}
```

**`forEach` **method will call the function provided once for each element in the array preserving the order. This function provided can take in 3 different arguments: `element`, `index`, `array`. Be sure that the order in which you pass these parameters follow the order.
