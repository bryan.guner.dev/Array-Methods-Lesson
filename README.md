---
cover: .gitbook/assets/reduce (1).png
coverY: 0
description: 'Github Repo: https://github.com/bgoonz/Array-Methods-Lesson'
---

# Home



{% embed url="https://bryan-guner.gitbook.io/array-methods-lesson" %}
Public Web Address
{% endembed %}

{% embed url="https://github.com/bgoonz/Array-Methods-Lesson" %}
Corresponding Github Repository
{% endembed %}

{% embed url="https://replit.com/@bgoonz/array-methods#index.js" %}

<mark style="color:orange;">map</mark>(\[<mark style="background-color:red;">🌽, 🐮, 🐔</mark>], <mark style="background-color:blue;">cook</mark>) => <mark style="background-color:green;">\[🍿, 🍔, 🍳]</mark>

<mark style="color:orange;">filter</mark>(\[<mark style="background-color:red;">🍿, 🍔, 🍳</mark>], <mark style="background-color:blue;">isVegetarian</mark>) => <mark style="background-color:green;">\[🍿, 🍳]</mark>

<mark style="color:orange;">reduce</mark>(\[<mark style="background-color:red;">🍿, 🍳</mark>], <mark style="background-color:blue;">eat</mark>) => <mark style="background-color:green;">💩</mark>

![](<.gitbook/assets/Array Methods\_Instructor Position Assessment-1.png>) ![](<.gitbook/assets/Array Methods\_Instructor Position Assessment-2.png>) ![](<.gitbook/assets/Array Methods\_Instructor Position Assessment-3.png>) ![](<.gitbook/assets/Array Methods\_Instructor Position Assessment-4.png>)

## Table of contents

* [Home](https://bryan-guner.gitbook.io/array-methods-lesson/README)

### Methods

### 👨💻 Methods

* [Page 1](https://bryan-guner.gitbook.io/array-methods-lesson/methods-1/page-1)
* [Array.forEach()](https://bryan-guner.gitbook.io/array-methods-lesson/methods-1/array.foreach)
* [Array.filter()](https://bryan-guner.gitbook.io/array-methods-lesson/methods-1/array.filter)

### Aux Information

* [Array Methods Explained As Emojis](https://bryan-guner.gitbook.io/array-methods-lesson/aux-information/array-methods-explained-as-emojis)
* [Array.reduce()](https://bryan-guner.gitbook.io/array-methods-lesson/aux-information/array.reduce)
* [Array.map()](https://bryan-guner.gitbook.io/array-methods-lesson/aux-information/array.map)

### Background

* [Mutability](https://bryan-guner.gitbook.io/array-methods-lesson/background/mutability)
* [Array Basics](https://bryan-guner.gitbook.io/array-methods-lesson/background/array-basics)
* [🔙 Background](https://bryan-guner.gitbook.io/array-methods-lesson/background/background)
* [Callback Functions](https://bryan-guner.gitbook.io/array-methods-lesson/background/callback-functions)
* [Higher Order Functions](https://bryan-guner.gitbook.io/array-methods-lesson/background/higher-order-functions)
* [🧘 Difference Between Functions & Methods...](https://bryan-guner.gitbook.io/array-methods-lesson/background/difference-between-functions-and-methods...)
* [➡ Fat Arrow Syntax](https://bryan-guner.gitbook.io/array-methods-lesson/background/fat-arrow-syntax)

### 📖 Resources

* [Array Methods Explained In Emojis](https://bryan-guner.gitbook.io/array-methods-lesson/resources/array-methods-explained-in-emojis)
* [Does It Mutate¿](https://bryan-guner.gitbook.io/array-methods-lesson/resources/does-it-mutate)
* [Cheat Sheet](https://bryan-guner.gitbook.io/array-methods-lesson/resources/cheat-sheet)
* [Other Array Methods](https://bryan-guner.gitbook.io/array-methods-lesson/resources/other-array-methods)

### 👽 Miscellaneous

* [Array Methods Explained As Emojis](https://bryan-guner.gitbook.io/array-methods-lesson/miscellaneous/array-methods-explained-as-emojis)

```
// Mutating
push()      // Insert an element at the end
pop()       // Remove an element from the end
unshift()   // Inserts an element in the beginning
shift()     // Remove first element
// Iterating
forEach()   // Iterates an array
filter()    // Iterates an array and result is filtered array
map()       // Iterates an array and result is new array
reduce()    // "Reduces" the array into single value (accumulator)
// Others
slice()     // Returns desired elements in a new array
concat()    // Append one or more arrays with given array
```
