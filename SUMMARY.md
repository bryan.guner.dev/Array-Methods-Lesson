# Table of contents

* [Home](README.md)

## 👨💻 Methods

* [🔗 Array.forEach()](methods/array.foreach.md)
* [🚮 Array.filter()](methods/array.filter.md)
* [💩 Array.reduce()](methods/array.reduce.md)
* [🗺 Array.map()](methods/array.map.md)
* [Native Implementation](methods/native-implementation.md)

## Background

* [😶 Mutability](background/mutability.md)
* [🍼 Array Basics](background/array-basics.md)
* [🔙 Background](background/background.md)
* [📲 Callback Functions](background/callback-functions.md)
* [🏔 Higher Order Functions](background/higher-order-functions.md)
* [🧘 Difference Between Functions & Methods...](background/difference-between-functions-and-methods....md)
* [➡ Fat Arrow Syntax](background/fat-arrow-syntax.md)

## 📖 Resources

* [💱 Does It Mutate¿](resources/does-it-mutate.md)
* [📰 Cheat Sheet](resources/cheat-sheet/README.md)
  * [👷♀ 👷♀ Examples](resources/cheat-sheet/examples.md)
* [🤓 Other Array Methods](resources/other-array-methods.md)
* [Examples](resources/examples/README.md)
  * [Refactor](resources/examples/refactor.md)

## 👽 Miscellaneous

* [🍍 Array Methods Explained As Emojis](miscellaneous/array-methods-explained-as-emojis.md)
